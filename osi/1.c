#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdatomic.h>

atomic_int ok = 0;

struct args {
  double *a;
  int n;
  int k;
  int p;
};

void *thr(void *argp) {
  struct args *arg = (struct args *)argp;
  for (int i = 0; i < arg->n; i++) {
    for (int j = i; j < arg->n; j++) {
      if ((i * arg->n + j - i*(i+1)/2) % arg->p == arg->k) {
        if (*(arg->a + i * arg->n + j) != *(arg->a + j * arg->n + i)) {
          printf("thread %d: [%d][%d] f\n", arg->k, i, j);
          atomic_fetch_add(&ok, 1);
        } else {
          printf("thread %d: [%d][%d]\n", arg->k, i, j);
        }
      }
    }
  }
}

int main() {
  int THREADS = 6;
  double a[4][4] = {
    {1, 2, 3, 5},
    {2, 0, 6, 8},
    {3, -6, 9, 12},
    {5, 8, 1.2, 16}
  };

  pthread_t tids[4];
  for (int i = 0; i < THREADS; i++) {
    struct args *arg = (struct args*) malloc(sizeof(struct args));
    arg->a = &a[0][0];
    arg->n = 4;
    arg->k = i;
    arg->p = THREADS;
    pthread_create(&(tids[i]), NULL, thr, arg);
  }
  for (int i = 0; i < THREADS; i++) {
    pthread_join(tids[i], NULL);
  }
  printf("%d\n", ok);
  return 0;
}
