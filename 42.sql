DROP DATABASE IF EXISTS two;
CREATE DATABASE two;
USE two;

CREATE TABLE `numbers` (
	id INT NOT NULL AUTO_INCREMENT,
    val INT NOT NULL,
    
    PRIMARY KEY (id)
);

DELIMITER //
CREATE PROCEDURE Fill()
BEGIN
	DECLARE i INT DEFAULT 0;
	l: LOOP
		IF i = 50 THEN 
			LEAVE l;
		END IF;
        SET i = i + 1;
        INSERT INTO `numbers` VALUES (null, RAND() * 99 + 1);
	END LOOP l;
END //

CREATE PROCEDURE Stats(
	OUT average FLOAT,
	OUT median FLOAT,
    OUT std_dev FLOAT,
	OUT average1 FLOAT,
	OUT median1 FLOAT,
    OUT stddev1 FLOAT
)
BEGIN
	DECLARE avg_, min_, max_, stddev_ FLOAT DEFAULT 0;
    DECLARE id_, val_, halfsize_ INT;
    DECLARE finished INT DEFAULT 0;
    DECLARE cur CURSOR FOR
		SELECT * FROM `numbers`;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
    
    SELECT CEIL(COUNT(*)/2) FROM numbers INTO halfsize_;
    
    SELECT AVG(val), MIN(val), MAX(val), STDDEV(val) FROM `numbers` INTO average, min_, max_, std_dev;
	SELECT
	(
	 (SELECT MAX(val) FROM
	   (SELECT val FROM numbers ORDER BY val LIMIT halfsize_) AS BottomHalf)
	 +
	 (SELECT MIN(val) FROM
	   (SELECT val FROM numbers ORDER BY val DESC LIMIT halfsize_) AS TopHalf)
	) / 2
	INTO median;
    
    DROP  TABLE IF EXISTS tmp;
    CREATE  TABLE tmp (
		id INT NOT NULL,
		val_mod INT NOT NULL,
        
        PRIMARY KEY (id)
    );
    
	OPEN cur;
    l: LOOP
        FETCH cur INTO id_, val_;
		IF finished = 1 THEN
			LEAVE l;
        END IF;
        IF val_ > average THEN
			INSERT INTO tmp VALUES (id_, min_);
        ELSE
			INSERT INTO tmp VALUES (id_, max_);			
        END IF;
	END LOOP l;
    CLOSE cur;
    SELECT AVG(val_mod), STDDEV(val_mod) FROM tmp INTO average1, stddev1;
	SELECT
	(
	 (SELECT MAX(val_mod) FROM
	   (SELECT val_mod FROM tmp ORDER BY val_mod LIMIT halfsize_) AS BottomHalf)
	 +
	 (SELECT MIN(val_mod) FROM
	   (SELECT val_mod FROM tmp ORDER BY val_mod DESC LIMIT halfsize_) AS TopHalf)
	) / 2
	INTO median1;
END //
DELIMITER ;

CALL Fill;
CALL Stats(@a, @b, @c, @d, @e, @f);
SELECT @a, @b, @c, @d, @e, @f;