USE `netexam`;

SELECT * FROM `exams`;
SELECT * FROM `exams` WHERE teacherId=1;
SELECT DISTINCT teacherId FROM `exams`;

SELECT name, timeInMinutes / questionsCountPerExam as timePerQuestion FROM `exams`;


SELECT * FROM `users` WHERE middleName IS NULL;

SELECT * FROM `answers` WHERE questionId=1 ORDER BY number ASC;
SELECT * FROM `exams` ORDER BY timeInMinutes DESC;

SELECT COUNT(*) FROM `exams` WHERE teacherId=1;
SELECT AVG(questionsCountPerExam) FROM `exams` WHERE semester=3;
SELECT MIN(correct) FROM `solutions` WHERE examId=1;
SELECT MAX(correct) FROM `solutions` WHERE examId=1;

SELECT result, COUNT(*) from `solution_details` WHERE solutionId=1 GROUP BY result;

