DROP DATABASE IF EXISTS one;
CREATE DATABASE one;
USE one;

CREATE TABLE `matches` (
	id INT NOT NULL AUTO_INCREMENT,
    `date` DATE NOT NULL,
    opponent NVARCHAR(50) NOT NULL,
    result TINYINT NOT NULL,
    
    PRIMARY KEY (id)
);

INSERT INTO `matches` VALUES
(null, '2019-01-31', '', 1),
(null, '2019-01-30', '', 1),
(null, '2019-01-29', '', 0),
(null, '2019-01-28', '', -1),
(null, '2019-01-27', '', 0),
(null, '2019-01-26', '', 0),
(null, '2019-01-25', '', 0),
(null, '2019-01-24', '', 1),
(null, '2019-01-23', '', 1),
(null, '2019-01-22', '', 1),
(null, '2019-01-21', '', 1);

DELIMITER //
CREATE PROCEDURE GetLongestStreaks(
	OUT wins INT,
    OUT losses INT,
    OUT draws INT
)
BEGIN
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE match_result TINYINT;
	DECLARE Lwinstreak, Llosestreak, Ldrawstreak INT DEFAULT 0;
	DECLARE winstreak, losestreak, drawstreak INT DEFAULT 0;
	
	DECLARE cur CURSOR FOR
		SELECT result FROM `matches` ORDER BY `date` ASC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
	OPEN cur;
    
	l: LOOP
		FETCH cur INTO match_result;
		IF finished = 1 THEN 
			LEAVE l;
		END IF;
        CASE
        WHEN match_result > 0 THEN
			SET winstreak = winstreak + 1;
            IF winstreak > Lwinstreak THEN
				SET Lwinstreak = winstreak;
			END IF;
			SET losestreak = 0;
			SET drawstreak = 0;
		WHEN match_result = 0 THEN
			SET drawstreak = drawstreak + 1;
            IF drawstreak > Ldrawstreak THEN
				SET Ldrawstreak = drawstreak;
			END IF;
			SET winstreak = 0;
			SET losestreak = 0;
		WHEN match_result < 0 THEN
			SET losestreak = losestreak + 1;
            IF losestreak > Llosestreak THEN
				SET Llosestreak = losestreak;
			END IF;
			SET winstreak = 0;
			SET drawstreak = 0;
        END CASE;
	END LOOP l;
    
	CLOSE cur;
    SET wins = Lwinstreak;
    SET losses = Llosestreak;
    SET draws = Ldrawstreak;
END //
DELIMITER ;


CALL GetLongestStreaks(@w, @l, @d);
SELECT @w, @l, @d;