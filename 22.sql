DROP DATABASE IF EXISTS two;
CREATE DATABASE two;
USE two;

CREATE TABLE teams
(
    id      INT         NOT NULL AUTO_INCREMENT,
    team    VARCHAR(50) NOT NULL,
    trainer VARCHAR(50) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE writers
(
    id     INT         NOT NULL AUTO_INCREMENT,
    writer VARCHAR(50) NOT NULL,
    genre  VARCHAR(50) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE tournament
(
    id      INT         NOT NULL AUTO_INCREMENT,
    team    VARCHAR(50) NOT NULL,
    trainer VARCHAR(50) NOT NULL,
    place   INT         NOT NULL,
    points  INT         NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO teams
VALUES (NULL, "North wolves", "R.Faith");
INSERT INTO teams
VALUES (NULL, "North wolves", "G.Homer");
INSERT INTO teams
VALUES (NULL, "North wolves", "A.Reigns");
INSERT INTO teams
VALUES (NULL, "North wolves", "S.Yohaman");
INSERT INTO teams
VALUES (NULL, "North wolves", "T.Trainer");

INSERT INTO teams
VALUES (NULL, "Purple abyss", "G.Homer");
INSERT INTO teams
VALUES (NULL, "Purple abyss", "T.Trainer");
INSERT INTO teams
VALUES (NULL, "Purple abyss", "S.Yohaman");
INSERT INTO teams
VALUES (NULL, "Purple abyss", "M.Meltdown");

INSERT INTO teams
VALUES (NULL, "Younger guard", "T.Trainer");
INSERT INTO teams
VALUES (NULL, "Younger guard", "A.Reigns");
INSERT INTO teams
VALUES (NULL, "Younger guard", "J.Joestar");
INSERT INTO teams
VALUES (NULL, "Younger guard", "G.Homer");

INSERT INTO teams
VALUES (NULL, "Saint Fusion", "G.Homer");

INSERT INTO teams
VALUES (NULL, "Dark Decode", "A.Reigns");
INSERT INTO teams
VALUES (NULL, "Dark Decode", "S.Yohaman");

INSERT INTO teams
VALUES (NULL, "Blade Spirit", "G.Roger");


INSERT INTO writers
VALUES (NULL, "Littner", "Dystopia");
INSERT INTO writers
VALUES (NULL, "Littner", "Romance");
INSERT INTO writers
VALUES (NULL, "Littner", "Fantasy");
INSERT INTO writers
VALUES (NULL, "Littner", "SCI-FI");
INSERT INTO writers
VALUES (NULL, "Littner", "Drama");
INSERT INTO writers
VALUES (NULL, "Littner", "Space western");

INSERT INTO writers
VALUES (NULL, "Horseman", "Dystopia");
INSERT INTO writers
VALUES (NULL, "Horseman", "Space western");
INSERT INTO writers
VALUES (NULL, "Horseman", "SCI-FI");

INSERT INTO writers
VALUES (NULL, "Mereman", "Dystopia");
INSERT INTO writers
VALUES (NULL, "Mereman", "Fantasy");
INSERT INTO writers
VALUES (NULL, "Mereman", "Romance");
INSERT INTO writers
VALUES (NULL, "Mereman", "SCI-FI");

INSERT INTO writers
VALUES (NULL, "Wired", "Romance");
INSERT INTO writers
VALUES (NULL, "Wired", "SCI-FI");
INSERT INTO writers
VALUES (NULL, "Wired", "Drama");

INSERT INTO writers
VALUES (NULL, "Uexkull", "SCI-FI");


INSERT INTO tournament
VALUES (NULL, "North wolves", "R.Faith", 2, 120);
INSERT INTO tournament
VALUES (NULL, "North wolves", "G.Homer", 3, 79);
INSERT INTO tournament
VALUES (NULL, "North wolves", "A.Reigns", 1, 161);
INSERT INTO tournament
VALUES (NULL, "North wolves", "S.Yohaman", 4, 100);
INSERT INTO tournament
VALUES (NULL, "North wolves", "T.Trainer", 5, 95);

INSERT INTO tournament
VALUES (NULL, "Purple abyss", "G.Homer", 3, 91);
INSERT INTO tournament
VALUES (NULL, "Purple abyss", "T.Trainer", 1, 147);
INSERT INTO tournament
VALUES (NULL, "Purple abyss", "S.Yohaman", 2, 124);
INSERT INTO tournament
VALUES (NULL, "Purple abyss", "M.Meltdown", 2, 149);

INSERT INTO tournament
VALUES (NULL, "Younger guard", "T.Trainer", 1, 132);
INSERT INTO tournament
VALUES (NULL, "Younger guard", "A.Reigns", 2, 118);
INSERT INTO tournament
VALUES (NULL, "Younger guard", "G.Roger", 3, 110);
INSERT INTO tournament
VALUES (NULL, "Younger guard", "J.Joestar", 1, 170);

INSERT INTO tournament
VALUES (NULL, "Saint Fusion", "G.Homer", 3, 147);

INSERT INTO tournament
VALUES (NULL, "Dark Decode", "A.Reigns", 4, 80);
INSERT INTO tournament
VALUES (NULL, "Dark Decode", "S.Yohaman", 5, 89);

INSERT INTO tournament
VALUES (NULL, "Blade Spirit", "G.Roger", 5, 55);
INSERT INTO tournament
VALUES (NULL, "Blade Spirit", "G.Homer", 1, 190);



SELECT DISTINCT team
FROM teams AS t1
WHERE (SELECT COUNT(*) FROM teams AS t2 WHERE t1.team = t2.team) > 1;

SELECT DISTINCT w1.writer
FROM writers AS w1
WHERE (SELECT COUNT(DISTINCT writer, genre) FROM writers AS w2 WHERE w1.writer = w2.writer) =
      (SELECT COUNT(DISTINCT genre) FROM writers);

SELECT DISTINCT w1.genre
FROM writers AS w1
WHERE (SELECT COUNT(DISTINCT writer, genre) FROM writers AS w2 WHERE w1.genre = w2.genre) =
      (SELECT COUNT(DISTINCT writer) FROM writers);

SELECT DISTINCT trainer
FROM teams
WHERE trainer NOT IN (SELECT DISTINCT trainer FROM teams WHERE team = "Purple abyss");

SELECT trainer
FROM tournament
GROUP BY trainer
HAVING AVG(points) >
       (SELECT AVG(avgp) FROM (SELECT AVG(points) AS avgp FROM tournament GROUP BY trainer) AS averages);

SELECT DISTINCT t1.team
FROM tournament AS t1
         JOIN tournament AS t2 ON t1.team = t2.team AND t1.trainer <> t2.trainer AND t1.place = 1 AND t2.place = 1;

SELECT DISTINCT t1.team
FROM tournament AS t1
         JOIN tournament AS t2 ON t1.team <> t2.team AND t1.trainer = t2.trainer AND t1.place <> 1 AND t2.place = 1;