DROP DATABASE IF EXISTS two;
CREATE DATABASE two;
USE two;

CREATE TABLE teams
(
    id      INT         NOT NULL AUTO_INCREMENT,
    team    VARCHAR(50) NOT NULL,
    trainer VARCHAR(50) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE writers
(
    id     INT         NOT NULL AUTO_INCREMENT,
    writer VARCHAR(50) NOT NULL,
    genre  VARCHAR(50) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE tournament
(
    id      INT         NOT NULL AUTO_INCREMENT,
    team    VARCHAR(50) NOT NULL,
    trainer VARCHAR(50) DEFAULT NULL,
    place   INT         NOT NULL,
    points  INT         NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO teams
VALUES (NULL, "North wolves", "R.Faith");
INSERT INTO teams
VALUES (NULL, "North wolves", "G.Homer");
INSERT INTO teams
VALUES (NULL, "North wolves", "A.Reigns");
INSERT INTO teams
VALUES (NULL, "North wolves", "S.Yohaman");
INSERT INTO teams
VALUES (NULL, "North wolves", "T.Trainer");

INSERT INTO teams
VALUES (NULL, "Purple abyss", "G.Homer");
INSERT INTO teams
VALUES (NULL, "Purple abyss", "T.Trainer");
INSERT INTO teams
VALUES (NULL, "Purple abyss", "S.Yohaman");
INSERT INTO teams
VALUES (NULL, "Purple abyss", "M.Meltdown");

INSERT INTO teams
VALUES (NULL, "Younger guard", "T.Trainer");
INSERT INTO teams
VALUES (NULL, "Younger guard", "A.Reigns");
INSERT INTO teams
VALUES (NULL, "Younger guard", "J.Joestar");
INSERT INTO teams
VALUES (NULL, "Younger guard", "G.Homer");

INSERT INTO teams
VALUES (NULL, "Saint Fusion", "G.Roger");

INSERT INTO teams
VALUES (NULL, "Dark Decode", "A.Reigns");
INSERT INTO teams
VALUES (NULL, "Dark Decode", "S.Yohaman");

INSERT INTO teams
VALUES (NULL, "Blade Spirit", "G.Roger");
INSERT INTO teams
VALUES (NULL, "Blade Spirit", "G.Homer");


INSERT INTO writers
VALUES (NULL, "Littner", "Dystopia");
INSERT INTO writers
VALUES (NULL, "Littner", "Romance");
INSERT INTO writers
VALUES (NULL, "Littner", "Fantasy");
INSERT INTO writers
VALUES (NULL, "Littner", "SCI-FI");
INSERT INTO writers
VALUES (NULL, "Littner", "Drama");
INSERT INTO writers
VALUES (NULL, "Littner", "Space western");

INSERT INTO writers
VALUES (NULL, "Horseman", "Dystopia");
INSERT INTO writers
VALUES (NULL, "Horseman", "Space western");
INSERT INTO writers
VALUES (NULL, "Horseman", "SCI-FI");

INSERT INTO writers
VALUES (NULL, "Mereman", "Dystopia");
INSERT INTO writers
VALUES (NULL, "Mereman", "Fantasy");
INSERT INTO writers
VALUES (NULL, "Mereman", "Romance");
INSERT INTO writers
VALUES (NULL, "Mereman", "SCI-FI");

INSERT INTO writers
VALUES (NULL, "Wired", "Romance");
INSERT INTO writers
VALUES (NULL, "Wired", "SCI-FI");
INSERT INTO writers
VALUES (NULL, "Wired", "Drama");

INSERT INTO writers
VALUES (NULL, "Uexkull", "SCI-FI");
INSERT INTO writers
VALUES (NULL, "Uexkull", "SCI-FI");


INSERT INTO tournament
VALUES (NULL, "North wolves", "R.Faith", 2, 120);
INSERT INTO tournament
VALUES (NULL, "North wolves", "G.Homer", 3, 79);
INSERT INTO tournament
VALUES (NULL, "North wolves", "A.Reigns", 1, 161);
INSERT INTO tournament
VALUES (NULL, "North wolves", "S.Yohaman", 4, 100);
INSERT INTO tournament
VALUES (NULL, "North wolves", "T.Trainer", 5, 95);

INSERT INTO tournament
VALUES (NULL, "Purple abyss", "G.Homer", 3, 91);
INSERT INTO tournament
VALUES (NULL, "Purple abyss", "T.Trainer", 1, 147);
INSERT INTO tournament
VALUES (NULL, "Purple abyss", "S.Yohaman", 2, 124);
INSERT INTO tournament
VALUES (NULL, "Purple abyss", "M.Meltdown", 2, 149);

INSERT INTO tournament
VALUES (NULL, "Younger guard", "T.Trainer", 1, 132);
INSERT INTO tournament
VALUES (NULL, "Younger guard", "A.Reigns", 2, 118);
INSERT INTO tournament
VALUES (NULL, "Younger guard", "G.Roger", 3, 110);
INSERT INTO tournament
VALUES (NULL, "Younger guard", "J.Joestar", 1, 170);

INSERT INTO tournament
VALUES (NULL, "Saint Fusion", "G.Homer", 3, 147);

INSERT INTO tournament
VALUES (NULL, "Dark Decode", "A.Reigns", 4, 80);
INSERT INTO tournament
VALUES (NULL, "Dark Decode", "S.Yohaman", 5, 89);

INSERT INTO tournament
VALUES (NULL, "Blade Spirit", "G.Roger", 5, 55);
INSERT INTO tournament
VALUES (NULL, "Blade Spirit", "G.Homer", 1, 190);


#1
SELECT *
FROM tournament;

SELECT ALL team
FROM tournament;

SELECT DISTINCT team
FROM tournament;

SELECT team, trainer,
                CASE
                    WHEN place = 1 THEN "winner"
                    WHEN place IN (2, 3) THEN "runner-up"
                    ELSE "loser"
                    END AS result
FROM tournament;

SELECT team, MAX(t1.points) - t1.points AS diff FROM tournament AS t1 GROUP BY team;

SELECT team, trainer, place FROM tournament WHERE place BETWEEN 2 AND 3;

SELECT DISTINCT team, trainer FROM tournament WHERE trainer IS NOT NULL;

SELECT DISTINCT trainer FROM tournament WHERE trainer LIKE "G.%";

SELECT DISTINCT LOWER(team) FROM tournament;

SELECT team, trainer, place FROM tournament WHERE place IN (2, 3); 
#

SELECT team, trainer, place FROM tournament ORDER BY place ASC;

SELECT team, trainer, place FROM tournament ORDER BY place DESC;

SELECT DISTINCT trainer FROM tournament AS t2 WHERE NOT EXISTS(SELECT trainer, place FROM tournament AS t1 WHERE t1.trainer = t2.trainer AND t1.place = 1);

SELECT SUM(points), AVG(points), MIN(points), MAX(points) FROM tournament;

SELECT trainer, AVG(points) AS avgp FROM tournament GROUP BY trainer HAVING avgp > 120;

#2.1

CREATE TABLE dividend (
    a INT NOT NULL,
    b INT NOT NULL,
    PRIMARY KEY (a, b)
);

CREATE TABLE divider (
    b INT NOT NULL,
    PRIMARY KEY (b)
);

INSERT INTO dividend VALUES
(1, 1),
(1, 2),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3);

INSERT INTO divider VALUES
(1),
(2),
(3);

SELECT DISTINCT d1.a FROM dividend AS d1
WHERE NOT EXISTS (
SELECT r.b FROM divider AS r WHERE r.b NOT IN (SELECT d2.b FROM dividend AS d2 WHERE d2.a = d1.a)
);

SELECT * FROM writers;
DELETE w1 FROM writers AS w1 JOIN writers AS w2 WHERE w1.id < w2.id AND w1.writer=w2.writer AND w1.genre=w2.genre;

#2.2
SELECT DISTINCT team
FROM teams AS t1
WHERE (SELECT COUNT(*) FROM teams AS t2 WHERE t1.team = t2.team) > 1;

SELECT DISTINCT w1.writer
FROM writers AS w1
WHERE (SELECT COUNT(DISTINCT writer, genre) FROM writers AS w2 WHERE w1.writer = w2.writer) =
      (SELECT COUNT(DISTINCT genre) FROM writers);

SELECT DISTINCT w1.genre
FROM writers AS w1
WHERE (SELECT COUNT(DISTINCT writer, genre) FROM writers AS w2 WHERE w1.genre = w2.genre) =
      (SELECT COUNT(DISTINCT writer) FROM writers);

SELECT DISTINCT trainer
FROM teams
WHERE trainer IN (SELECT DISTINCT trainer FROM teams WHERE team = "Purple abyss");

SELECT trainer
FROM tournament
GROUP BY trainer
HAVING AVG(points) >
       (SELECT AVG(avgp) FROM (SELECT AVG(points) AS avgp FROM tournament GROUP BY trainer) AS averages);

SELECT DISTINCT t1.team
FROM tournament AS t1
         JOIN tournament AS t2 ON t1.team = t2.team AND t1.trainer <> t2.trainer AND t1.place < 4 AND t2.place < 4;

SELECT DISTINCT t1.team
FROM tournament AS t1
         JOIN tournament AS t2 ON t1.team <> t2.team AND t1.trainer = t2.trainer AND t1.place <> 1 AND t2.place = 1;


#3
CREATE INDEX idx_team ON tournament (team);
CREATE INDEX idx_trainer ON tournament (trainer);


SET @total = 0;
CREATE TRIGGER ins_total BEFORE INSERT ON tournament
FOR EACH ROW
SET @total = @total + NEW.points;

CREATE TRIGGER upd_total BEFORE UPDATE ON tournament
FOR EACH ROW
SET @total = @total + NEW.points - OLD.points;

CREATE TRIGGER del_total BEFORE DELETE ON tournament
FOR EACH ROW
SET @total = @total - OLD.points;

SELECT @total;
INSERT INTO tournament
VALUES (NULL, "Blade Spirit", "G.Roger", 5, 55);
SELECT @total;
INSERT INTO tournament
VALUES (NULL, "Blade Spirit", "G.Homer", 1, 190);
SELECT @total;
SELECT * FROM tournament;
UPDATE tournament SET points=60 WHERE id=19;
DELETE FROM tournament WHERE id=19;

DELIMITER //
CREATE PROCEDURE CountWins (
	IN team_ VARCHAR(50),
    OUT wins INT
)
BEGIN
	SELECT COUNT(*) FROM tournament WHERE team = team_ AND place = 1 INTO wins;
END//

CREATE FUNCTION CountWinsF (
	team_ VARCHAR(50)
) RETURNS INT
BEGIN
	SELECT COUNT(*) FROM tournament WHERE team = team_ AND place = 1 INTO @res;
    RETURN @res;
END//

DELIMITER ;

CALL CountWins("Younger guard", @w);
SELECT @w;
SELECT team, CountWinsF(team) FROM tournament GROUP BY team;