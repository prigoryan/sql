DROP DATABASE IF EXISTS `netexam`;
CREATE DATABASE `netexam`;
USE `netexam`;

CREATE TABLE `users` (
  id         INT                         NOT NULL AUTO_INCREMENT,
  firstName  NVARCHAR(128)               NOT NULL,
  lastName   NVARCHAR(128)               NOT NULL,
  middleName NVARCHAR(128)                        DEFAULT NULL,
  login      NVARCHAR(128)               NOT NULL,
  password   NVARCHAR(128) BINARY        NOT NULL,
  userType   ENUM ('STUDENT', 'TEACHER') NOT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (login)
);

CREATE TABLE `students` (
  userId   INT           NOT NULL,
  semester INT           NOT NULL,
  `group`  NVARCHAR(128) NOT NULL,

  PRIMARY KEY (userId),
  FOREIGN KEY (userId) REFERENCES `users` (id)
    ON DELETE CASCADE
);

CREATE TABLE `teachers` (
  userId     INT           NOT NULL,
  department NVARCHAR(128) NOT NULL,
  position   NVARCHAR(128) NOT NULL,

  PRIMARY KEY (userId),
  FOREIGN KEY (userId) REFERENCES `users` (id)
    ON DELETE CASCADE
);

CREATE TABLE `exams` (
  id                    INT          NOT NULL AUTO_INCREMENT,
  teacherId             INT          NOT NULL,
  name                  NVARCHAR(64) NOT NULL,
  semester              INT          NOT NULL,
  ready                 BIT          NOT NULL,
  questionsCountPerExam INT                   DEFAULT NULL,
  timeInMinutes         INT                   DEFAULT NULL,
  showDetails           BIT                   DEFAULT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (semester, name),
  FOREIGN KEY (teacherId) REFERENCES `teachers` (userId)
    ON DELETE CASCADE
);

CREATE TABLE `questions` (
  id       INT NOT NULL   AUTO_INCREMENT,
  examId   INT NOT NULL,
  question NVARCHAR(4096) DEFAULT NULL,
  number   INT NOT NULL,
  correct  INT            DEFAULT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (examId) REFERENCES `exams` (id)
    ON DELETE CASCADE
);

CREATE TABLE `answers` (
  id         INT NOT NULL   AUTO_INCREMENT,
  questionId INT NOT NULL,
  answer     NVARCHAR(1024) DEFAULT NULL,
  number     INT NOT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (questionId, number),
  FOREIGN KEY (questionId) REFERENCES `questions` (id)
    ON DELETE CASCADE
);

CREATE TABLE `current_attempts` (
  id        INT NOT NULL AUTO_INCREMENT,
  studentId INT NOT NULL,
  examId    INT NOT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (studentId, examId),
  FOREIGN KEY (studentId) REFERENCES `students` (userId)
    ON DELETE CASCADE,
  FOREIGN KEY (examId) REFERENCES `exams` (id)
    ON DELETE CASCADE
);

CREATE TABLE `attempt_answers` (
  id             INT NOT NULL   AUTO_INCREMENT,
  attemptId      INT NOT NULL,
  questionId     INT NOT NULL,
  questionNumber INT NOT NULL,
  answerNumber   INT            DEFAULT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (attemptId, questionNumber),
  FOREIGN KEY (attemptId) REFERENCES `current_attempts` (id)
    ON DELETE CASCADE,
  FOREIGN KEY (questionId) REFERENCES `questions` (id)
    ON DELETE CASCADE
);

CREATE TABLE `solutions` (
  id        INT NOT NULL AUTO_INCREMENT,
  studentId INT NOT NULL,
  examId    INT NOT NULL,
  correct   INT NOT NULL,
  wrong     INT NOT NULL,
  noAnswer  INT NOT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (studentId, examId),
  FOREIGN KEY (studentId) REFERENCES `students` (userId)
    ON DELETE CASCADE,
  FOREIGN KEY (examId) REFERENCES `exams` (id)
    ON DELETE CASCADE
);

CREATE TABLE `solution_details` (
  id             INT                             NOT NULL AUTO_INCREMENT,
  solutionId     INT                             NOT NULL,
  questionNumber INT                             NOT NULL,
  result         ENUM ('YES', 'NO', 'NO_ANSWER') NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (solutionId) REFERENCES `solutions` (id)
);

CREATE TABLE `sessions` (
  token  VARCHAR(40) NOT NULL,
  userId INT         NOT NULL,

  PRIMARY KEY (token),
  UNIQUE KEY (userId),
  FOREIGN KEY (userId) REFERENCES `users` (id)
    ON DELETE CASCADE
);
